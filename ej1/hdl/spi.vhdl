--------------------------------------------------------------------------------
--                              Interfaz SPI                                  --
--------------------------------------------------------------------------------
-- File name      : spi.vhdl
--
-- Purpose        : Interfaz SPI para controlar con AXI.
--
-- Dependencies   : ieee.std_logic_1164
--
-- Author         : Joaquin Ulloa
--
-- Comments       : Permite adaptar una interfaz AXI a una SPI, la primera
--                funciona como esclavo y la segunda como maestro.
--                Tiene implementados solo 2 registros, pero de ser necesario se
--                pueden agregar más sin ningún problema.
--------------------------------------------------------------------------------

library ieee;
   use ieee.std_logic_1164.all;

entity spi is
    generic (
        C_AXI_DATA_WIDTH : integer :=32;
        C_AXI_ADDR_WIDTH : integer :=4
    );
    port (
        AXI_CLK            : in  std_logic;
        AXI_RESETN         : in  std_logic;
        -- to/from AXI_REGISTERS
        WDATA_I             : in  std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
        RDATA_O             : out std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
        WENA_I              : in  std_logic;
        RENA_I              : in  std_logic;
        RADDR_I             : in  std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
        WADDR_I             : in  std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
        -- SPI Signals
        MOSI_O              : out std_logic;
        MISO_I              : in  std_logic;
        SCLK_O              : out std_logic;
        CSn_O               : out std_logic;
        -- IRQ
        INT_O               : out std_logic
    );
end entity;

architecture arch of spi is

    -- Estados de la FSM de control SPI
    type spi_states is (
        ST_IDLE,
        ST_START,
        ST_RD_WR
    );
    signal spi_st       : spi_states;

    -- Direcciones de los registros de configuración
    constant ADDR_REG_TX    : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"0";
    constant ADDR_REG_RX    : std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0) := x"4";
    -- Tamaño de datos y contadores
    constant SPI_WIDTH      : integer :=  8;
    constant CLKS_RATE      : integer := 50; -- clk_axi / 2*clk_spi
    -- COmunicación SPI
    signal wr_data          : std_logic_vector(SPI_WIDTH-1 downto 0);
    signal rd_data          : std_logic_vector(SPI_WIDTH-1 downto 0);
    signal rd_data_reg      : std_logic_vector(SPI_WIDTH-1 downto 0);
    signal spi_mosi         : std_logic;
    signal spi_sclk         : std_logic;
    signal spi_cs_n         : std_logic;
    signal int              : std_logic;
    signal bits_send        : integer range SPI_WIDTH downto 0 := 0;
    -- Generación del clock SPI
    signal spi_clk_en       : std_logic;
    signal spi_clk_en_d     : std_logic;
    signal clk_cnt          : integer range 0 to CLKS_RATE-1;
    
begin

    ----------------------------------------------------------------------------
    -- FSM de control de SPI, funciona con el clock AXI y los datos se envian
    -- según el clock enable del SPI.
    -- Warning: spi_clk_en se usa con ambos flancos.
    p_rd_wr_fsm:
    process (AXI_CLK, AXI_RESETN) begin
        if (AXI_RESETN = '0') then
            spi_st      <= ST_IDLE;
            spi_cs_n    <= '1';
            spi_mosi    <= '0';
            spi_sclk    <= '0';
            int         <= '0';
            bits_send   <=  0;
            wr_data     <= (others => '0');
            rd_data     <= (others => '0');
        elsif (rising_edge(AXI_CLK)) then
            case spi_st is
                when ST_IDLE =>
                    spi_cs_n    <= '1';
                    spi_mosi    <= '0';
                    spi_sclk    <= '0';
                    int         <= '0';
                    bits_send   <=  0;
                    if (WENA_I = '1') then
                        if (WADDR_I = x"0") then
                            spi_st      <= ST_START;
                            wr_data  <= WDATA_I(SPI_WIDTH-1 downto 0);
                        end if;
                    end if;
                    if (spi_clk_en = '1' and spi_clk_en_d = '0') then
                        int         <= '0';
                    end if;
                when ST_START =>
                    if (spi_clk_en = '1' and spi_clk_en_d = '0') then
                        spi_cs_n    <= '0';
                        bits_send   <=  0;
                        spi_st      <= ST_RD_WR;
                        spi_sclk    <= '0'; -- redundante
                        spi_mosi    <= wr_data(SPI_WIDTH-1);
                    end if;
                when ST_RD_WR =>
                    if (spi_clk_en = '0' and spi_clk_en_d = '1') then
                        if (bits_send <= SPI_WIDTH-1) then
                            spi_sclk    <= not spi_sclk;
                            rd_data(SPI_WIDTH-bits_send-1) <= MISO_I;
                        end if;
                    elsif (spi_clk_en = '1' and spi_clk_en_d = '0') then
                        if (bits_send <= SPI_WIDTH-2) then
                            spi_sclk    <= not spi_sclk;
                            bits_send   <= bits_send + 1;
                            spi_mosi    <= wr_data(SPI_WIDTH-bits_send-2);
                        else
                            bits_send   <=  0; -- redundante
                            spi_cs_n    <= '1';
                            spi_sclk    <= '0';
                            int         <= '1';
                            spi_st      <= ST_IDLE;
                            rd_data_reg <= rd_data;
                        end if;
                    end if;
            end case;
        end if;
    end process;

    ---------------------------------------------------------------------------
    -- Asignación de salidas
    MOSI_O      <= spi_mosi;
    SCLK_O      <= spi_sclk;
    CSn_O       <= spi_cs_n;
    INT_O       <= int;
    RDATA_O(C_AXI_DATA_WIDTH-1 downto SPI_WIDTH)   <= (others => '0');
    RDATA_O(SPI_WIDTH-1 downto 0)   <= rd_data_reg  when (RADDR_I = x"4") else
                                       wr_data      when (RADDR_I = x"0") else
                                       (others => '0');
    
     ---------------------------------------------------------------------------
     -- Se genera la señal de habilitación del clock SPI, que comanda todo el
     -- comportamiento del mismo.
     p_clk_en_gen:
     process (AXI_CLK, AXI_RESETN) begin
        if (AXI_RESETN = '0') then
            spi_clk_en      <= '0';
            spi_clk_en_d    <= '0';
            clk_cnt         <=  0;
        elsif (rising_edge(AXI_CLK)) then
            spi_clk_en_d    <= spi_clk_en;
            if (clk_cnt = 25) then
                spi_clk_en      <= not spi_clk_en;
             end if;
            if (clk_cnt = CLKS_RATE-1) then
                clk_cnt         <= 0;
            else
                clk_cnt         <= clk_cnt + 1;
            end if;
        end if;
    end process;

end architecture;
