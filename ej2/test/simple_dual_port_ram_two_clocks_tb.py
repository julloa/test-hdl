import cocotb
import random

from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue

from cocotb.utils import get_sim_time

CLK_PERIOD_A = 2
CLK_PERIOD_B = 4


@cocotb.test()
def simple_test(dut):
    """
        Simple test: it writes 2 values and reads them back.
    """

    dut._log.info('Init Clocks')
    cocotb.fork(Clock(dut.CLKA, CLK_PERIOD_A, units='ns').start())
    cocotb.fork(Clock(dut.CLKB, CLK_PERIOD_B, units='ns').start())

    dut._log.info('Setup the initial state of signals')
    dut.ENA <= 1 # Always enable
    dut.ENB <= 1 # Always enable
    dut.WEA <= 0

    dut.ADDRA <= 0
    dut.ADDRB <= 0

    dut.DIA <= 0
    dut.DOB <= 0

    yield RisingEdge(dut.CLKA)
    yield RisingEdge(dut.CLKA)

    dut._log.info('Write value 0x0F10A in address 0x00100')
    dut.WEA <= 1
    dut.ADDRA <= 0x00100
    dut.DIA <= 0x0F10A
    yield RisingEdge(dut.CLKA)
    dut.WEA <= 0
    dut.ADDRB <= 0x00100
    yield ReadOnly()

    dut._log.info('Read value in address 0x00100')

    # It's a synch read memory, so you need 2 cycles to read the real data

    yield RisingEdge(dut.CLKB)
    yield RisingEdge(dut.CLKB)

    if (dut.DOB.value.integer != dut.DIA.value.integer):
        raise TestFailure(
            "Data read in address 0x00100 is not correct")

@cocotb.test()
def zeros_ones_test(dut):
    """
        Zeros Ones test: it writes all zeros and reads them back, then it does the same with ones.
    """

    dut._log.info('Init Clocks')
    cocotb.fork(Clock(dut.CLKA, CLK_PERIOD_A, units='ns').start())
    cocotb.fork(Clock(dut.CLKB, CLK_PERIOD_B, units='ns').start())

    dut._log.info('Setup the initial state of signals')
    dut.ENA <= 1 # Always enable
    dut.ENB <= 1 # Always enable
    dut.WEA <= 0

    dut.ADDRA <= 0
    dut.ADDRB <= 0

    dut.DIA <= 0
    dut.DOB <= 0

    yield RisingEdge(dut.CLKA)
    yield RisingEdge(dut.CLKA)

    dut._log.info('Writes all zeros')
    dut.WEA <= 1
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRA <= addr
        dut.DIA <= 0x00000
        yield RisingEdge(dut.CLKA)
    dut.WEA <= 0
    
    dut._log.info('Read all zeros')
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRB <= addr
        yield ReadOnly()

        # It's a synch read memory, so you need 2 cycles to read the real data
        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != 0x00000):
            raise TestFailure(
                "Data read in address {} is not correct".format(dut.ADDRB.value.integer))

    dut._log.info('Writes all ones')
    dut.WEA <= 1
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRA <= addr
        dut.DIA <= 0x11111
        yield RisingEdge(dut.CLKA)
    dut.WEA <= 0
    
    dut._log.info('Read all ones')
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRB <= addr
        yield ReadOnly()

        # It's a synch read memory, so you need 2 cycles to read the real data
        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != 0x11111):
            raise TestFailure(
                "Data read in address {} is not correct".format(dut.ADDRB.value.integer))

@cocotb.test()
def full_step_test(dut):
    """
        Full Step test: it writes the whole memory and reads it back.
    """

    dut._log.info('Init Clocks')
    cocotb.fork(Clock(dut.CLKA, CLK_PERIOD_A, units='ns').start())
    cocotb.fork(Clock(dut.CLKB, CLK_PERIOD_B, units='ns').start())

    dut._log.info('Setup the initial state of signals')
    dut.ENA <= 1 # Always enable
    dut.ENB <= 1 # Always enable
    dut.WEA <= 0

    dut.ADDRA <= 0
    dut.ADDRB <= 0

    dut.DIA <= 0
    dut.DOB <= 0

    yield RisingEdge(dut.CLKA)
    yield RisingEdge(dut.CLKA)

    dut._log.info('Write values x in [0,1024] in address 0x[x]')
    dut.WEA <= 1
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRA <= addr
        dut.DIA <= addr
        yield RisingEdge(dut.CLKA)
    dut.WEA <= 0
    
    dut._log.info('Read all values')
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRB <= addr
        yield ReadOnly()

        # It's a synch read memory, so you need 2 cycles to read the real data
        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != dut.ADDRB.value.integer):
            raise TestFailure(
                "Data read in address {} is not correct".format(dut.ADDRB.value.integer))


@cocotb.test()
def full_up_down_test(dut):
    """
        Full Up DOwn test: it writes the whole memory and reads it back two times, but this part is not sequential.
    """

    dut._log.info('Init Clocks')
    cocotb.fork(Clock(dut.CLKA, CLK_PERIOD_A, units='ns').start())
    cocotb.fork(Clock(dut.CLKB, CLK_PERIOD_B, units='ns').start())

    dut._log.info('Setup the initial state of signals')
    dut.ENA <= 1 # Always enable
    dut.ENB <= 1 # Always enable
    dut.WEA <= 0

    dut.ADDRA <= 0
    dut.ADDRB <= 0

    dut.DIA <= 0
    dut.DOB <= 0

    yield RisingEdge(dut.CLKA)
    yield RisingEdge(dut.CLKA)

    dut._log.info('Write values x in [0,1024] in address 0x[x]')
    dut.WEA <= 1
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRA <= addr
        dut.DIA <= addr
        yield RisingEdge(dut.CLKA)
    dut.WEA <= 0
    
    dut._log.info('Read all values')
    for addr in range(dut.MEM_SIZE.value.integer):
        dut.ADDRB <= addr
        yield ReadOnly()

        # It's a synch read memory, so you need 2 cycles to read the real data
        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != dut.ADDRB.value.integer):
            raise TestFailure(
                "Data read in address {} is not correct".format(dut.ADDRB.value.integer))

        dut.ADDRB <= dut.MEM_SIZE.value.integer-addr
        yield ReadOnly()

        # It's a synch read memory, so you need 2 cycles to read the real data
        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != dut.ADDRB.value.integer):
            raise TestFailure(
                "Data read in address {} is not correct".format(dut.ADDRB.value.integer))
