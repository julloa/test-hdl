## Ejercicio 3

### Intro

Este problema es más bien conceptual, requiere que propongas una solución a un
problema especifico, ideando una solución integral al mismo. La presentación de
la solución puede ser un diagrama en bloques con un resumen explicado, una
presentación, una descripción en VHDL/Verilog, lo que te sea mas útil para
contarnos tu idea.

Se evaluará:

* Creatividad en la idea propuesta.
* Cobertura de especificación.
* Claridad en la explicación.
* Recursos utilizados.

### Descripción del problema

Se dispone de un System on Chip (SoC) que contiene:

* Una zona de lógica programable (PL)
* Un controlador de memoria DDR4
* 4 Interfaces AXI3 de tamaño de burst máximo 256 transacciones y 128 bits de
ancho máximo por palabra.
* Una CPU que puede acceder a la memoria DDR4 y la zona de la PL.

Todo esto se esquematiza en la siguiente figura.

```
                         SOC
+-----------------------------------------------------+
|                                                     |
|                                                     |      +-------------+
|                                                     |      |             |
|  +------------------+            +---------------+  |      |             |
|  |                  |   AXIx4    |               |  |      |             |
|  |                  <------------>               |  |      |             |
|  |                  |            |               |  |      |             |
|  |                  <------------>    MEMORY     |  |      |             |
|  |        PL        |            |               <--------->     DDR4    |
|  |                  <------------>  CONTROLLER   |  |      |             |
|  |                  |            |               |  |      |             |
|  |                  <------------>               |  |      |             |
|  |                  |            |               |  |      |             |
|  +--------^---------+            +-------^-------+  |      |             |
|           |                              |          |      |             |
|           |                              |          |      |             |
|  +--------+------------------------------+-------+  |      +-------------+
|  |                                               |  |
|  |                    CPU                        |  |
|  |                                               |  |
|  |                                               |  |
|  +-----------------------------------------------+  |
+-----------------------------------------------------+
```

Se requiere diseñar un core en la PL lo mas simple posible que permita medir el
ancho de banda hacia la DDR4.

Se debe explicar como es el método para hacer esta medición, en qué consiste, 
que recursos cree que necesitará y como se puede garantizar que lo medido es 
realmente el ancho de banda máximo de la memoria. A su vez indicar las 
limitaciones del método elegido.

## Solución

### Funcionamiento

La idea es colocar un módulo por canal que "sniffer" la interfaz AXI, mida el ancho de banda de cada una y luego envíe la información "cruda" al PS, para que éste calcule el ancho de banda total.

[![Diagrama en bloques](https://gitlab.com/julloa/test-hdl/-/tree/master/ej3/block_diagram.png)](https://gitlab.com/julloa/test-hdl/-/tree/master/ej3/block_diagram.png)

Para medir el ancho de banda es necesario medir cantidad de datos transferidos y tiempo. Para lo primero, se toma la información de tamaño y largo que brinda el header del protocolo: AWSIZE/ARSIZE y AWLEN/ARLEN, tamaño de cada palabra y palabras por burst respectivamente. El producto de ambos datos da la cantidad total de bits enviados en una transacción, pero para optimizar recursos los datos se envían por separado al PS, quien deberá hacer la multiplicación. Para medir el tiempo se inicia un contador, que corre con el mismo clock que la interfaz AXI y cuyo enable serán los valid de los datos y se considerará que terminó la cuenta cuando se registre la señal de ready. Ambos datos de tamaño se enviarán inmediatamente al PS para aprovechar el tiempo para hacer las cuentas necesarias y el resultado del contador se enviará una vez terminada la transacción. Ver transacción AXI.
Se supone que se trata de un stream de datos que indica el start of packet, end of packet y data valid para cada transacción. De esta manera, sabiendo que se trata de palabras de 128 bits (que entran en una sola transacción) y suponiendo que en modo burst llega una palabra por ciclo de clock, solo sería necesario implementar un contador que se incremente con cada ciclo de clock mientras el data valid esté activo.
El sniffer estaría compuesto por tres partes: una FSM encargada de detectar inicio y fin de los paquetes y parsear toda la información contenida en los mismos, un contador de palabras y un contador de pulsos de clock totales por transacción. La FSM enviaría los pulsos de enable a los contadores y los contadores enviarían sus resultados al PS.

Una limitación intrínseca de la solución propuesta es que al tratarse de un sniffer, el módulo sólo puede monitorear el tráfico de datos, pero no puede ejercer ninguna acción sobre los mismos. La ventaja de esta característica es que al no estar en cascada con el data path no introduce latencia en los datos, por lo que la medición será más confiable.

[![Transacción AXI](https://gitlab.com/julloa/test-hdl/-/tree/master/ej3/AXI_write_transaction.svg)](https://gitlab.com/julloa/test-hdl/-/tree/master/ej3/AXI_write_transaction.svg)

### Recursos

Dado el requerimiento de usar "4 Interfaces AXI3 de tamaño de burst máximo 256 transacciones y 128 bits de ancho máximo por palabra" y suponiendo que se trata de una FPGA Xilinx con hard memory controller, por ejemplo de la familia Zynq-7000, se optó por usar los high-performance AXI ports, que se comunican con el multiport DRAM Controller por medio del bus AMBA Interconect. Bajo éstas suposiciones, se podría usar el [IP de Xilinx axi_interconnect](https://www.xilinx.com/support/documentation/ip_documentation/axi_interconnect/v2_1/pg059-axi-interconnect.pdf), ya que permite un burst length de hasta 256 palabras, palabras de hasta 1024 bits para AXI3 (y AXI4) y es soportado por la familia Zynq-7000.

[![Esquemático SoC Xilinx](https://gitlab.com/julloa/test-hdl/-/tree/master/ej3/Xilinx_SoC.png)](https://gitlab.com/julloa/test-hdl/-/tree/master/ej3/Xilinx_SoC.png)
